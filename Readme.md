## Mise en place d'un système automatisé de suivi et de gestion des tests de bout en bout.

Les tests de bout en bout (end-to-end testing en anglais) sont une technique utilisée pour vérifier si une application (site Internet, application mobile…) se comporte comme prévu du début à la fin. Le testeur doit se mettre dans la peau d’un utilisateur et dérouler les tests comme s’il utilisait véritablement l’outil mis à sa disposition. Cette technique permet de valider le fonctionnement du front, mais aussi de vérifier son intégration avec le back-office et autres webservices.

Il est également nécessaire de couvrir les aspects visuels du logiciel, qui sont extrêmement importants du point de vue de l'utilisateur, en effectuant des tests de régression visuelle.

L'ensemble de ces tests sont généralement exécutés après les tests fonctionnels et après avoir vérifié le fonctionnement du système. Il sont, autrement dit, réalisés juste avant la mise en production de l’application.

Ces tests sont obligatoire car ils sont les seuls permettant de vérifier le parfait fonctionnement de l’application du point de vue du métier.

C'est la raison pour laquelle la mise en place d'un système automatisé de suivi et de gestion de ces tests représente un avantage considérable pour votre équipe.


### I - Présentation des outils :

- **Jira** est un système de suivi de bugs, de gestion des incidents, et de gestion de projets certifié **ISO 27001, PCI-DSS**, s'adressant aux développeurs Agile de tout niveau. <br>
  https://www.atlassian.com/fr/software/jira.
  <br><br>
  C'est un outil de gestion évolutif compatible avec une centaines d'outils de développement et proposant plus de 1000 extensions parmi lesquels **Xray**.
  <br><br>

    - **Xray** est une application de gestion des tests de pointe permettant :
      <br>
        - Une gestion des tests manuels et automatisés en tant que problèmes Jira.
          Ces tests peuvent être spécifier en langage gherkin (cucumber), intégrez aux cadres d'automatisation des tests.
          <br><br>
        - La création des plans de test pour suivre un ensemble de tests et des exécutions de tests planifiées.
          Les tests peuvent être exécuté sur différents environnements et rapporter à l'aide de **l'API REST** grâce à l'outil **CI**.
          <br><br>
        - L'affichage d'une couverture de test des exigences avec des graphiques interactifs mais également une analyse du statut des entités de test par version, du plan de test et des environnements d'exécution.
          <br><br>
          https://marketplace.atlassian.com/apps/1211769/xray-test-management-for-jira?hosting=cloud&tab=overview
          <br><br>

-  **Cypress** est un framework de test javascript de nouvelle génération conçu pour le Web moderne permettant
   de tester facilement et rapidement tout application qui s'exécute dans un navigateur. <br>
   L'outil, permet l'écriture de tests unitaires, d'intégration et de **bout en bout**. <br>
   https://docs.cypress.io/guides/overview/why-cypress.html#In-a-nutshell
   <br><br>

- Avec **Cucumber et Gherkin**, vous pouvez facilement partager vos tests avec des coéquipiers non techniques, des clients, des parties prenantes pour montrer comment l'application se comporte dans des conditions spécifiques.
  De plus lorsque les demandes de fonctionnalités arrivent, vous pouvez les traduire de manière presque transparente en un test Cucumber tout de suite et commencer à développer !  
  <br>

    - **Cucumber** est un outil logiciel adapté au BDD « Behavior Driven Development » (Développement axé sur le comportement),
      proposant différentes implémentations pour de nombreux langages de programmation. <br>
      Cucumber permet de transformer des scénarios d'une « story » en tests automatisés. <br>
      Chaque étape d'un scénario est implémentée comme une méthode de test. <br>
      https://cucumber.io/
      <br><br>

    - **Gherkin** est un langage naturel facile de compréhension, d’utilisation permettant d’expliquer le déroulement d’une fonctionnalité ou d’une application sans rentrer dans les détails. <br>
      https://cucumber.io/docs/gherkin/reference/ <br>
      Ce langage existe dans plus de 60 langues dont l’anglais et le français.
      <br><br>

- **GitLab** est une plateforme permettant d'héberger et de gérer des projets web. <br>
  GitLab s'étend au test logiciel, au packaging d'applications, à l'intégration, au déploiement continus, à la configuration, jusqu'au monitoring et à la sécurité applicative. <br>
  https://gitlab.com/
  <br><br>
    - Gitlab **CI / CD** est un outil intégré à GitLab pour le développement logiciel à travers les méthodologies continues :
        - Intégration continue (CI)
        - Livraison continue (CD)
        - Déploiement continu (CD)
          <br>
          L'outil permet d’automatiser les builds, les tests, les déploiements... des applications. <br>
          https://docs.gitlab.com/ee/ci/README.html <br>
          https://blog.eleven-labs.com/fr/introduction-gitlab-ci/
          <br><br>
          
- **Applitools** est une plateforme de test visuel qui intègre des algorithmes avancés d'IA visuelle pour détecter les changements significatifs au niveau des applications testées.
   
   L'application permet d'analysez instantanément des pages entières de votre application et signalez les différences qui ne sont visibles que par l'œil humain. 
   Elle détecte automatiquement les différences à chaque fois que vous effectuez des tests et fournit une application web sophistiquée qui vous permet d'inspecter facilement les résultats des tests, de zoomer sur les changements visuels, de regrouper automatiquement les différences similaires et bien plus encore...
   <br>
   https://applitools.com/about/

### II - Présentation du fonctionnement :
    
#### a) Processus :

![](readme-screenshots/diagram.png)

#### b) Scénario :

Dans ce scénario, un projet a été créé dans Jira. Ce projet est une application web dotée d'un formulaire de connexion.

L'idée serait de concevoir des tests permettant de répondre à la story suivante :

![](readme-screenshots/story.PNG)

Voici 2 tests possibles :

![](readme-screenshots/tests-jira.PNG)

Ces tests contiennent des détails au format cucumber :

- **BTD-16**

![](readme-screenshots/detail-btd-16.PNG) 

- **BTD-17**

![](readme-screenshots/detail-btd-17.PNG)

**Remarque** : L'instruction "Lorsque je souhaite me connecter, je vais sur la page de connexion", pourrait être ajouté à un ticket de pré-condition (Xray) de manière à éviter les doublons. <br> https://docs.getxray.app/display/XRAY/Pre-Condition

Afin d'éxécuter ces tests, un environnement CI a été mis en place sur Gitlab. <br>

Il suffit donc d'effectuer ces tests dans Gitlab et de générer automatiquement un résultat de test dans Jira. 

- Une fois sur la page principale du dépôt de test du projet, il faut simplement cliquer sur ![](readme-screenshots/gitlab-pipelines.PNG) pour pouvoir éxécuter les différents tests.

![](readme-screenshots/gitlab-run.PNG) 

- Lancement des tests liés à la connexion : ![](readme-screenshots/play.PNG)

![](readme-screenshots/gitlab-run-end.PNG) 

- Après l'exécution des tests, un ticket **d'exécution de test** est généré dans le *backlog* du projet sur **Jira**.

![](readme-screenshots/backlog-jira.PNG) 
![](readme-screenshots/exec-jira.PNG) 

- Voici le contenu de ce ticket : 

 ![](readme-screenshots/exec-results-jira.PNG) 
  
- Un clique sur ![](readme-screenshots/icon-test-jira.PNG) permet d'obtenir les détails du test.

 ![](readme-screenshots/test-result-jira-success.PNG)
 
 ![](readme-screenshots/test-result-jira-failed.PNG)
 
- Le premier test étant un succès, une preuve visuelle a été envoyée à applitools par le biais de son api.

![](readme-screenshots/applitools.PNG)

Une image de référence est alors ajoutée et à chaque lancement de ce test, la nouvelle image est comparée afin de détecter ou non les changements.

**Voir :** https://applitools.com/tutorials/cypress.html pour mieux comprendre le fonctionnement.  
       
### III - Création des tests cucumber dans Jira

Une fois votre projet créé dans Jira, vous pouvez alors ajouter vos tests cucumber.

- Entrez dans votre projet.

- Cliquez sur **Tableau du Test** puis sur **Bibliothèque de test**. <br><br>

    ![](readme-screenshots/tableau-test.PNG)
    ![](readme-screenshots/biblio-test-jira.PNG)
  
Vous êtes alors dans la bibliothèque de test de votre projet. Vous pourrez y ajouter tous les tests qui s'y rapportent et les organiser comme vous le souhaitez en créant des répertoires. <br>
Un clic droit sur le répertoire **Bibliothèque de tests** vous permettra de voir toutes les possibilités disponibles.
 
![](readme-screenshots/creer-test.PNG)

Par exemple, on souhaite créer des tests liés au formulaire de connexion de l'application.

- Cliquez sur **Créer un dossier** et créez un dossier nommé **Connexion**.

![](readme-screenshots/connexion-jira.PNG)

- Ensuite, faite un clic droit sur le dossier **Connexion** et cliquez sur **Créer un test** pour créer votre test à l'intérieur.

Gardez le type **Xray Test**

![](readme-screenshots/type-test-xray.PNG)

Voici un exemple de résumé de test :

![](readme-screenshots/resume-test-jira.PNG)

- Une fois le test créé, ouvrez le.

Vous pouvez décrire le test, ajouter des détails, des conditions préalables et bien plus encore. <br>
Dans notre cas, nous modifierons les détails du test.

- Il faut choisir le format **cucumber** et ajoutez les instructions.

![](readme-screenshots/details-test.PNG)
    
- Cliquez sur ![](readme-screenshots/test-detail-button.PNG) pour afficher les détails du test s'ils sont cachés.

Voici 3 ressources en dehors de la documentation officielle (https://cucumber.io/docs/gherkin/), vous permettant de mieux comprendre le langage **Cherkin** et d'écrire de bons critères d'acceptation.

- Comprendre le langage : https://www.artza-technologies.com/blog/langage-gherkin

- Rédiger des scénarios avec Gherkin : https://www.jbvigneron.fr/parlons-dev/bdd-rediger-scenarios-avec-gherkin/

- Écrire de bons critères d'acceptation : https://www.artza-technologies.com/blog/comment-ecrire-bons-criteres-acceptation


### IV - Installation et configuration de l'environnement de test 

#### Prérequis :
- **npm** 

**npm** (Node Package Manager) est le gestionnaire de paquets officiel de Node.js. Il permet de télécharger et d’installer des paquets (encore appelés modules) pour pouvoir les utiliser pour un projet ou au contraire de partager des paquets pour que d’autres utilisateurs puissent les utiliser.
<br>

npm est aujourd’hui le plus grand registre de paquets au monde avec plus d’un million de paquets disponibles.

Vous aurez donc besoin de ce registre afin de pouvoir installer tous les paquets nécessaires.
<br><br>
https://www.npmjs.com/get-npm

#### a) Création de notre projet de test

Vous devez créer un répertoire pour les tests e2e de votre projet.

Une fois que cela est fait, ouvrez votre invite de commande.

Et rendez-vous dans ce répertoire :

```
cd /chemin/vers/votre/répertoire
```

Initialisez votre projet avec `npm init` et répondez aux questionnaires en ligne de commande.

- Le nom du projet
- La version
- Une description
- Et le point d'entrée (index.js par défaut), il suffit d'appuyer sur entrée.

Installez **Cypress**.

```
npm install cypress
```

https://docs.cypress.io/guides/getting-started/installing-cypress.html#Adding-npm-scripts

#### b) Installation des plugins nécessaires 

```
npm install cypress-cucumber-preprocessor
```

Le préprocesseur **cypress-cucumber** permet la prise en charge de l'utilisation des fichiers de fonctionnalités (**Gherkin**) lors des tests avec Cypress. <br> 
https://www.npmjs.com/package/cypress-cucumber-preprocessor/v/1.12.0

```
npm install cucumber-json-merge
```

**Cucumber-json-merge** permet de fusionner les résultats des tests **JSON Cucumber**. <br>
https://www.npmjs.com/package/cucumber-json-merge

```
npm install @applitools/eyes-cypress
```

Le SDK Applitools **Eyes Cypress** est un simple plugin pour Cypress. Une fois installé, il ajoute trois méthodes principales, **cy.eyesOpen**, **cy.eyesCheckWindow** et **cy.eyesClose**.
<br>
https://www.npmjs.com/package/@applitools/eyes-cypress

#### c) Ajout de scripts npm

L'ajout des scripts npm sera particulièrement utile lors de la mise en œuvre de l'intégration continue.

Ajoutez donc ses commandes au **scripts** champ de votre **package.json**.

```
  "scripts": {
    "cypress-open": "./node_modules/.bin/cypress open",
    "cypress-run": "./node_modules/.bin/cypress run",
    "cypress-run-login-test-in-chrome": "./node_modules/.bin/cypress run --spec 'cypress/integration/login.feature' --headless --browser chrome",
    "merge-cucumber-reports": "./node_modules/.bin/cucumber-json-merge -d 'cypress/reports/all' -o 'cypress/reports/merged-test-results.cucumber.json'"
  }
```

- **cypress-open** permet l'ouverture de cypress. 

    - Si un dossier **cypress** n'est pas automatiquement présent dans votre projet, lancez ce script pour ouvrir cypress et aussi pour générer ce dossier.
    ```
    npm run cypress-open
    ```
- **cypress-run** exécutera tous les tests sans entête dans le navigateur Electron (navigateur par défaut de cypress).
- **cypress-run-all-tests-in-chrome** exécutera tous les tests dans le navigateur chrome et forcera le navigateur à être masqué.
- **cypress-run-login-tests-in-chrome** exécutera tous les tests liés au login dans le navigateur chrome et forcera le navigateur à être masqué. <br><br>
https://docs.cypress.io/guides/guides/command-line.html


- **merge-cucumber-reports** permet de fusionner tous les rapports de test (cucumber json) contenus dans le répertoire ` cypress/reports/all ` en un seul rapport (merged-test-results.cucumber.json) placé dans ` cypress/reports/ `.

#### d) Configuration du préprocesseur cucumber

Ajoutez cette configuration à votre **package.json**.

```
  "cypress-cucumber-preprocessor": {
    "nonGlobalStepDefinitions": true,
    "cucumberJson": {
      "generate": true,
      "outputFolder": "cypress/reports/all",
      "filePrefix": "",
      "fileSuffix": ".cucumber"
    }
  }
```

- **nonGlobalStepDefinitions** : 
  - Si **true**, le modèle **Cypress Cucumber Preprocessor Style** sera utilisé pour placer les fichiers de définitions d'étape. 
  - Si **faux**, le style cucumber "oldschool" (tout est global) sera utilisé.
<br><br>
- **cucumberJson** est la configuration permettant à chaque lancement de test de générer un rapport pour chaque test effectué.
 <br> Ces rapports sont ensuite générés dans le répertoire `cypress/reports/all` avec le préfixe "**.cucumber**".
  
  
L'étape suivante consiste à relier le plugin **cypress-cucumber-preprocessor** à cypress.

`cypress/plugins/index.js`

```
const cucumber = require('cypress-cucumber-preprocessor').default

module.exports = (on, config) => {
    on('file:preprocessor', cucumber())
}
```

Il faudra maintenant configurer Cypress.

`cypress.json`

```
{
  "baseUrl": "https://url/de/votre/projet",
  "testFiles": "**/*.feature",
  "ignoreTestFiles": "*.js",
  "video": false,
  "screenshotOnRunFailure": false
}
```

- **baseUrl** (facultatif) permet de spécifier l'url de notre application.
- **testFiles: "\*\*/\*.feature\"** permet de spéficier à cypress que les fichiers `.feature` sont désormais les principaux fichiers de tests.
- **ignoreTestFiles** (facultatif) permet de ne pas afficher les fichiers javascript et de n'afficher que les fichiers `.feature` lors d'un **npm run cypress-open**.
- **video** && **screenshotOnRunFailure**(facultatif) permettent la génération ou non de captures d'écran et/ou de vidéos pendant l'exécution des tests.

https://www.npmjs.com/package/cypress-cucumber-preprocessor/v/1.12.0


#### e) Configuration du SDK Eyes Cypress 

Pour configurer ce plugin et ses commandes, il suffit d'exécuter la commande `npx eyes-setup` à partir de votre invite de commande.

Pour l'installation manuelle, voir : https://www.npmjs.com/package/@applitools/eyes-cypress

#### f) Écriture de test cypress au format cucumber

La manière d'organiser les fonctionnalités et le code javascript est assez simple.

Voici un exemple des tests liés au formulaire de connexion :

##### Structure du dossier `integration` :

![](readme-screenshots/structure-test.PNG)

##### Le fichier `login.feature` contient alors les scénarios de test écrits en **Cherkin**. 

```
# language: fr

Fonctionnalité: En tant qu'utilisateur, je souhaite me connecter afin d'accéder à l'application
        
    Contexte: Connexion : Pour me connecter
        Lorsque je souhaite me connecter, je vais sur la page de connexion
    
    @BTD-16
    Scénario: Connexion : Je soumets le formulaire avec des données non valides
      Quand je remplis le champ e-mail avec une adresse e-mail non valide
      Et que je remplis le champ mot de passe avec un mot de passe non valide
      Et que je soumets le formulaire
      Alors j'ai le message d'erreur qui apparaît
    
    @BTD-17
    Scénario: Connexion : Je soumets le formulaire avec des données valides
      Quand je remplis le champ e-mail avec une adresse e-mail valide
      Et que je remplis le champ mot de passe avec un mot de passe valide
      Et que je soumets le formulaire
      Alors je suis bien connecté à l'application
```

Dans cet exemple :
- Le français est choisi comme langue par défaut.
- **Fonctionnalité** (Feature) rassemble tous les scénarios envisagés et est présenté sous la forme d'une description (user story). 
- Le contexte représente une condition préalable qui sera appelée avant chaque scénario.
- Chaque scénario représente alors un cas de test et comprend des instructions.

Vous avez sans doute remarqué les **décorateurs** **@BTD-16** et **@BTD-17** au dessus de chaque **Scénario**.
 
Ces décorateurs représentent l'**identifiant** des tests situés sur **Jira**.

![](readme-screenshots/tests-jira.PNG)

L'identifiant doit être spécifié dans notre fichier `.feature` pour éviter la duplication des tests dans **Jira** lors de l'intégration.

##### Le dossier login peut contenir tous les fichiers .js relatifs aux tests de connexion.

Dans l'exemple, un seul fichier `login.js` est créé, contenant les tests relatifs à `login.feature`.
Mais il est possible d'ajouter plusieurs fichiers `.js`, ils seront tous parcourus lors de l'exécution des tests.

Voici le contenu de `login.js`

```
import { When, And, Then } from "cypress-cucumber-preprocessor/steps";

const loginSelectors = require('../../selectors/login.json')
const accountSelectors = require('../../selectors/account.json')
const usersFixtures = require('../../fixtures/users.json')

When("je souhaite me connecter, je vais sur la page de connexion", () => {
    cy.visit('/')
    cy.eyesOpen({
        appName : 'Mon application',
        batchName: 'Mon application : Connexion'
    })
})

When("je remplis le champ e-mail avec une adresse e-mail valide", () => {
    cy.get(loginSelectors.emailField).type(usersFixtures.valid.email)
})

And("je remplis le champ mot de passe avec un mot de passe non valide", () => {
    cy.get(loginSelectors.passwordField).type(usersFixtures.invalid.password)
})

When("je remplis le champ e-mail avec une adresse e-mail non valide", () => {
    cy.get(loginSelectors.emailField).type(usersFixtures.invalid.email)
})

And("je remplis le champ mot de passe avec un mot de passe valide", () => {
    cy.get(loginSelectors.passwordField).type(usersFixtures.valid.password)
})

And("je soumets le formulaire", () => {
    cy.get(loginSelectors.loginButton).click({force: true})
})

Then("j'ai le message d'erreur qui apparaît", () => {
    cy.get(loginSelectors.errorMessage)
    cy.eyesCheckWindow()
    cy.eyesClose()
})

Then("je suis bien connecté à l'application", () => {
    cy.get(loginSelectors.loginPage).should('not.exist')
    cy.get(accountSelectors.userProfileButton)
    cy.get(accountSelectors.logoutButton)
    cy.eyesCheckWindow()
    cy.eyesClose()
})
````

Vous remarquerez ici que le `cypress-cucumber-preprocessor` fournit des méthodes 
liées aux instructions dans `login.feature` (When, And, Then ...). <br>
https://github.com/TheBrainFamily/cypress-cucumber-preprocessor

Le premier paramètre de ces méthodes doit nécessairement être égal (===) à l'intrusion testée. 

Le second est alors une fonction, contenant des tests classiques de cypress. <br>
https://docs.cypress.io/guides/getting-started/writing-your-first-test.html

Vous remarquerez également la présence des méthodes **cy.eyesOpen** pour démarrer le test, **cy.eyesCheckWindow** pour prendre des captures d'écran (pour chaque étape de test) et **cy.eyesClose** pour fermer le test. 

A noter : 

- Les méthodes **describe()** et **it()** ne sont plus utiles !
 
- De plus, pour éviter la duplication, les sélecteurs d'éléments **html** sont placés dans un fichier json. <br>
C'est aussi le cas pour les données utilisées. 

En résumé, la création d'un test cucumber cypress revient à :
- Créer un fichier `.feature` à la racine du dossier `integration` contenant les scénarios de test (Cypress - Gherkin).
- Créer un dossier portant le même nom que ce fichier.
- Puis, ajouter un ou plusieurs fichiers `.js` contenant les tests (Cypress - Cucumber).

#### g) Configuration du fichier CI (Gitlab)

Il est temps d'envoyer notre projet de test sur **gitlab** afin de procéder à l'intégration.

Mais d'abord, Il faudra configurer l"intégration continue. 
Dans un premier temps, créer à la racine de votre projet un fichier `.gitlab-ci.yml` qui va lister les différentes tâches à effectuer.
Lorsque GitLab détecte un fichier `.gitlab-ci.yml`, il va automatiquement essayer d'exécuter les étapes avec des runners.

Ajoutez les lignes suivantes :

```
default:
  image: cypress/browsers:node12.13.0-chrome80-ff74

  before_script:
    - npm install
    - export APPLITOOLS_API_KEY=$applitools_api_key

  after_script:
    - |
        npm run merge-cucumber-reports
        token=$(curl -H "Content-Type: application/json" -X POST --data "{ \"client_id\": \"$xray_api_client_id\",\"client_secret\": \"$xray_api_client_secret\" }" https://xray.cloud.xpand-it.com/api/v1/authenticate| tr -d '"')
        curl -H "Content-Type: application/json" -X POST -H "Authorization: Bearer $token" --data @cypress/reports/merged-test-results.cucumber.json https://xray.cloud.xpand-it.com/api/v1/import/execution/cucumber
        echo "Done !!!"

Connexion:
  rules:
    - when: manual
  script:
    - npm run cypress-run-login-test-in-chrome
```

https://docs.getxray.app/display/XRAY/Integration+with+GitLab

- L'image docker **cypress/browsers:node12.13.0-chrome80-ff74** permettra d'effectuer les tests dans un navigateur chrome 80.
- Avant l'exécution **(before_script)**, les dépendances du projet sont installées, la clé API d'applitools est définie. 
- Après l'exécution **(after_script)**, les rapports de tests sont fusionnés, puis envoyés à Jira via l'api Xray.
- **rules: - when: manual**, permet d'éviter l'exécution des tests à chaque commit.
- Les tests sont lancés **(script)**...

https://docs.getxray.app/display/XRAY/Import+Execution+Results+-+REST 

Vous remarquez sûrement la présence des variables **$applitools_api_key** (before_script), **$xray_api_client_id** et **$xray_api_client_secret** (after_script).
 
Il s'agit de variables d'environnement à configurer dans gitlab (non écrites dans le code pour des raisons évidentes de sécurité car ce sont des variables liées à vos clés d'Api **Applitools et Xray**).

Il est temps de créer votre dépôt, et d'envoyer vos tests sur gitlab ! <br>
https://docs.gitlab.com/ee/gitlab-basics/create-project.html

Une fois votre projet de test sur gitlab, ajoutez les variables d'environnement **$applitools_api_key**, **$xray_api_client_id** et du **$xray_api_client_secret**.

- Cliquez sur votre projet.

- Cliquez sur **settings**, puis sur **CI / CD**.

![](readme-screenshots/config-ci-gitlab.PNG) 

- Cliquez ensuite sur **Collapse** et sur **Add variable** pour ajouter vos 2 variables d'environnements.

![](readme-screenshots/gitlab-variable.PNG) 

Ça y est, le processus d'automatisation des tests est prêt, il ne reste plus qu'à exécuter vos tests dans votre environnement CI (Gitlab) ! 	&#10004;

---

Par **Dalil Chablis** (www.dalilchablis.com).



